var default_card = document.getElementById("default_card");

function create_task(button){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/create_task?user={{user.uuid}}', false);
    xhr.send();

    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }
    location.reload();
}

function delete_task(button){
    var card = button.parentElement.parentElement;
    var id = card.id;
    console.log(card);
    card.remove();
    var xhr = new XMLHttpRequest();
    var data = JSON.stringify({"task_id": id});
    xhr.open('POST', '/delete_task?user={{user.uuid}}', false);
    xhr.send(data);
    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }
}

function update_title(title){
    var card = title.parentElement.parentElement;
    var id = card.id;

    var xhr = new XMLHttpRequest();
    var data = JSON.stringify({"task_id": id, "title": title.value});

    xhr.open('POST', '/update_task?user={{user.uuid}}', false);
    xhr.send(data);
    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }
}

function update_text(text){
    var card = text.parentElement.parentElement;
    var id = card.id;

    var xhr = new XMLHttpRequest();
    var data = JSON.stringify({"task_id": id, "text": text.value});

    xhr.open('POST', '/update_task?user={{user.uuid}}', false);
    xhr.send(data);
    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }
}

function complete_task(button){
    var card = button.parentElement.parentElement;
    var id = card.id;

    var xhr = new XMLHttpRequest();
    var data = JSON.stringify({"task_id": id, "doned": true});

    xhr.open('POST', '/update_task?user={{user.uuid}}', false);
    xhr.send(data);
    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }

    card.className = "card bg-success shadow mb-4 text-center";
    button.className = "btn btn-warning";
    button.text = "Uncomplete";
    button.onclick = function() {uncomplete_task(button);};
}

function uncomplete_task(button){
    var card = button.parentElement.parentElement;
    var id = card.id;

    var xhr = new XMLHttpRequest();
    var data = JSON.stringify({"task_id": id, "doned": false});

    xhr.open('POST', '/update_task?user={{user.uuid}}', false);
    xhr.send(data);
    if (xhr.status != 200) {
        alert("Error"); // пример вывода: 404: Not Found
    }

    card.className = "card bg-light shadow mb-4 text-center";
    button.className = "btn btn-success";
    button.text = "Complete";
    button.onclick = function() {complete_task(button);};
}