from uuid import uuid4

import peewee as peew

db = peew.SqliteDatabase('database.db')


class User(peew.Model):
    class Meta:
        database = db
    
    id = peew.PrimaryKeyField()
    name = peew.TextField(null=False, unique=True)
    password = peew.TextField(null=True)
    uuid = peew.TextField(index=True, null=False)


class Task(peew.Model):
    class Meta:
        database = db
    
    id = peew.PrimaryKeyField()
    user = peew.ForeignKeyField(User, 'id', db_column='user_id')
    title = peew.TextField(default="New Task", null=False)
    text = peew.TextField(default="", null=False)
    doned = peew.BooleanField(default=False, null=False)




# Подключение к базе данных
db.connect()
db.create_tables([User, Task])

try:
    print(User.create(name="user", password="user", uuid=str(uuid4().hex)))
except:
    pass