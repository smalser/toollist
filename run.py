import json
from flask import Flask
from flask import render_template, send_from_directory, redirect, request, url_for, jsonify, abort
from models import User, Task


app = Flask(__name__, "/assets", "templates/assets")


'''
# Для работы тимплейтовских штук
@app.route('/assets/<path:path>')
@app.route('/result/assets/<path:path>')
def send_js(path):
    return send_from_directory('templates/assets/', path)
'''

#  Главная страница дашборда
@app.route('/', redirect_to='/index')
@app.route('/index', methods = ["GET"])
def index_page(): 
    uuid = request.args.get('user', None)
    try:
        user = User.get(uuid=uuid)
    except:
        return redirect(url_for('.login', message='User not found.'))

    # Тельце
    tasks = list(Task.filter(user=user))
    tasks.sort(key=lambda x: x.doned)
    print(tasks)
    return render_template('index.html', user=user, tasks=tasks)


# Страничка логина
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html', message='')
    elif request.method == "POST":
        try:
            name = request.form.get('name')
            password = request.form.get('password')
            user = User.get(name=name, password=password)
        except:
            return render_template('login.html', message='Wrong username.')
        return redirect(url_for('.index_page', user=user.uuid))


# Получение пользователя из запроса
def get_user(request):
    uuid = request.args.get('user', None)
    print(uuid)
    try:
        return User.get(uuid=uuid)
    except:
        return redirect(url_for('.login', message='User not found.'))

def success():
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 


# Апишные методы
# Создание задачи
@app.route('/create_task', methods=['POST'])
def create_task():
    user = get_user(request)

    Task.create(user=user)
    return redirect(url_for('.index_page', user=user.uuid))


# Удаление задачи
@app.route('/delete_task', methods=['POST'])
def delete_task():
    user = get_user(request)
    data = json.loads(request.data,encoding='utf-8')
    print(request.data.decode('utf-8'))
    task_id = data.get('task_id', None)
    task = Task.get(user=user, id=int(task_id[4:]))
    task.delete_instance()

    return success()


# Изменение полей
@app.route('/update_task', methods=['POST'])
def update_task():
    user = get_user(request)
    data = json.loads(request.data, encoding='utf-8')
    print(data)
    params = {param: data.get(param, None) for param in ['task_id', 'title', 'text', 'doned']}
    task = Task.get(user=user, id=int(params['task_id'][4:]))

    for param, value in params.items():
        if value is not None:
            print(param, value)
            setattr(task, param, value)
    task.save()
    return success()


if __name__ == "__main__":
    app.run(debug=True)